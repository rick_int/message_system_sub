const rabbit = require('amqplib');
const controller = require('./api/controller');

let url = process.env.RABBIT_URL || 'amqp://localhost';

const do_consume = async () => {
    try {
        let connection = await rabbit.connect(url);

        let channel = await connection.createChannel();
    
        let queue = 'worker_log';
        await channel.assertQueue(queue, {
            durable: true
        });
        await channel.prefetch(1);
    
        console.log(" SUB APP Connected to rabbitmq. Ready to consume messages.")
        console.log(" [*] Waiting for logs in %s. To exit press CTRL+C", queue);
    
        await channel.consume(queue, async (msg) => {
    
            let secs = msg.content.toString().length - 1;
    
            console.log(" [x] Received '%s'", msg.content.toString());
    
            await controller.saveMessageToDB(secs);
            channel.ack(msg);
        }, {
            noAck: false
        });
    }  catch(error) {
        console.log('Error during rabbitmq connection: %s', error.message);
        setTimeout(() => {
            do_consume();
        }, 10 * 1000);
    }
}

do_consume();
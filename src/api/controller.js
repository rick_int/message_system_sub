let controllers = {
    saveMessageToDB: async (secs) => {

        const updateDB = new Promise((resolve, reject) => {

            console.log(' [ ] Inserting message to database...')
            setTimeout(() => {
                console.log(' [x] Done');
                resolve(true);
            }, (secs/4) * 1000 );

        });
        await updateDB;
        return true;
    }
};

module.exports = controllers;
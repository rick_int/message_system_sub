const rabbit = require('amqplib');
const controller = require('./api/controller');

let args = process.argv.slice(2);
if (args.length == 0) {
    console.log("Usage: receive_message.js <warning>.<severity>")
    process.exit(1);
}

let url = process.env.RABBIT_URL || 'amqp://localhost';

const do_consume = async () => {
    try {
        let conn = rabbit.connect(url);

        const channel = await conn.createChannel();

        let exchange = 'task_logs';
        await channel.assertExchange(exchange, 'topic', {
            durable: false
        });

        let q = await channel.assertQueue('', {
            exclusive: true
        });

        console.log(" [*] Waiting for logs. To exit press CTRL+C");

        for await (key of args) {
            channel.bindQueue(q.queue, exchange, key);
        }

        await channel.consume(q.queue, async (msg) => {

            let secs = msg.content.toString().length - 1;

            console.log(" [x] %s: %s", msg.fields.routingKey, msg.content.toString());

            await controller.saveMessageToDB(secs);
            channel.ack(msg);
        }, {
            noAck: false
        });

    } catch (error) {
        console.log('Error during rabbitmq connection: %s', error.message);
        setTimeout(() => {
            do_consume();
        }, 10 * 1000);
    }
};

do_consume();
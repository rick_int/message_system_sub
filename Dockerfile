FROM node:12-alpine

RUN mkdir /sub/

WORKDIR /sub/

COPY ./ /sub/
RUN npm install

ENV RABBIT_URL=amqp://guest:guest@rabbitmq

ENTRYPOINT [ "node", "src/receive_logs.js" ]